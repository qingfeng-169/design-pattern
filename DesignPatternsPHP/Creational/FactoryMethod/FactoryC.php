<?php


namespace DesignPatterns\Creational\FactoryMethod;

class FactoryC implements Factory
{
    public function createProduct(): Product
    {
        return new ProductC();
    }
}