<?php


namespace DesignPatterns\Creational\FactoryMethod;


class FactoryA implements Factory
{
    public function createProduct(): Product
    {
        return new ProductA();
    }
}