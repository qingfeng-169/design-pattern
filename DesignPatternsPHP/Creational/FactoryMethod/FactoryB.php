<?php


namespace DesignPatterns\Creational\FactoryMethod;


class FactoryB implements Factory
{
    public function createProduct(): Product
    {
        return new ProductB();
    }
}