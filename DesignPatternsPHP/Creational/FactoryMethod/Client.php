<?php


namespace DesignPatterns\Creational\FactoryMethod;


class Client
{
    public function main()
    {
        $factory = new FactoryA();
        $factory->createProduct();
        $factory = new FactoryB();
        $factory->createProduct();
        $factory = new FactoryC();
        $factory->createProduct();
    }
}