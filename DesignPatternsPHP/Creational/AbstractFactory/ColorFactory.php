<?php


namespace DesignPatterns\Creational\AbstractFactory;


class ColorFactory extends AbstractFactory
{
    public function getColor(string $color): Color
    {
        if ($color == 'RED') {
            return new Red();
        } elseif ($color == 'GREEN') {
            return new Green();
        } elseif ($color == 'BLUE') {
            return new Blue();
        } else {
            return;
        }
    }

    public function getShape(string $shape): Shape
    {
    }

}