<?php


namespace DesignPatterns\Creational\AbstractFactory;


class Square implements Shape
{
    public function draw(): void
    {
        echo 'Inside Square::draw() method<br />';
    }
}