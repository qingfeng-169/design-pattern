<?php


namespace DesignPatterns\Creational\AbstractFactory;


class Blue implements Color
{

    public function fill(): void
    {
        echo 'Blue::fill()<br />';
    }
}