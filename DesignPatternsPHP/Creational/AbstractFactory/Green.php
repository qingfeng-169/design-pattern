<?php


namespace DesignPatterns\Creational\AbstractFactory;


class Green implements Color
{

    public function fill(): void
    {
        echo 'Green::fill()<br />';
    }
}