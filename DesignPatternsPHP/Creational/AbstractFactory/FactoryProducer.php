<?php


namespace DesignPatterns\Creational\AbstractFactory;


class FactoryProducer
{
    public static function getFactory(string $choice): AbstractFactory
    {
        if ($choice == 'SHAPE') {
            return new ShapeFactory();
        } else if ($choice == 'COLOR') {
            return new ColorFactory();
        } else {
            return;
        }
    }
}