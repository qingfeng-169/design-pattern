<?php


namespace DesignPatterns\Creational\AbstractFactory;


class Red implements Color
{

    public function fill(): void
    {
        echo 'Red::fill()<br />';
    }
}