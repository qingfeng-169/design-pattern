<?php


namespace DesignPatterns\Creational\AbstractFactory;


interface Shape
{
    public function draw(): void;
}