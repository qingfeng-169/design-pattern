<?php


namespace DesignPatterns\Creational\AbstractFactory;


interface Color
{
    public function fill(): void;
}