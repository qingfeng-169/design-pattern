<?php


namespace DesignPatterns\Creational\AbstractFactory;


class Circle implements Shape
{
    public function draw(): void
    {
        echo 'Inside Circle::draw() method<br />';
    }
}