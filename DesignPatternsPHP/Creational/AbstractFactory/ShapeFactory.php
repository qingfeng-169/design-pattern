<?php


namespace DesignPatterns\Creational\AbstractFactory;


class ShapeFactory extends AbstractFactory
{
    public function getColor(string $color): Color
    {
    }

    public function getShape(string $shape): Shape
    {
        if ($shape == 'CIRCLE') {
            return new Circle();
        } elseif ($shape == 'RECTANGLE') {
            return new Rectangle();
        } elseif ($shape == 'SQUARE') {
            return new Square();
        } else {
            return;
        }
    }


}