<?php


namespace DesignPatterns\Creational\AbstractFactory;


abstract class AbstractFactory
{
    public abstract function getColor(string $color): Color;

    public abstract function getShape(string $shape): Shape;
}