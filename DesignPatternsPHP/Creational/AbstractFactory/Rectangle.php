<?php


namespace DesignPatterns\Creational\AbstractFactory;


class Rectangle implements Shape
{

    public function draw(): void
    {
        echo 'Inside Rectangle::draw() method<br />';
    }
}