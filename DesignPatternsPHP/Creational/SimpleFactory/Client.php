<?php


namespace DesignPatterns\Creational\SimpleFactory;


class Client
{
    public function index()
    {
        $productA = Factory::create("ProductA");
        $productB = Factory::create("ProductA");
    }
}