<?php

namespace DesignPatterns\Creational\SimpleFactory;

class Factory
{
    /**
     * @param string $productName
     *
     * @return Product|null
     */
    public static function create(string $productName)
    {
        switch ($productName) {
            case 'ProductA':
                $obj = new ProductA();
                break;
            case 'ProductB':
                $obj = new ProductB();
                break;
            default:
                $obj = null;
                break;
        }

        return $obj;
    }
}